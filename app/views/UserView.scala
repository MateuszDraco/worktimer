package views

import models.users.UserData
import models.works.WorkState

/**
 *
 * User: mateusz
 * Date: 28.03.2014
 * Time: 07:17
 * Created with IntelliJ IDEA.
 */
case class UserView(login: String, profile: UserData, state: WorkState)