package views.helpers

import views.html.helpers.SemanticUIFieldConstructor

/**
 *
 * User: mateusz
 * Date: 01.04.2014
 * Time: 07:41
 * Created with IntelliJ IDEA.
 */
object MyHelpers {

  import views.html.helper.FieldConstructor

  implicit val myFields = FieldConstructor(SemanticUIFieldConstructor.f)
}
