package models.tasks

/**
 *
 * User: mateusz
 * Date: 08.03.2014
 * Time: 14:09
 * Created with IntelliJ IDEA.
 */
object Events {

  case class CreateTask(user: String, name: String, description: String)
  case class StartTask(user: String, task: String)
  case class StopTask(user: String, task: String)
  case class TrashTask(user: String, task: String)
  case class RequestAll(user: String)
}
