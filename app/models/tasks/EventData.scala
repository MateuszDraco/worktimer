package models.tasks

import scala.util.Random

/**
 *
 * User: mateusz
 * Date: 08.03.2014
 * Time: 14:11
 * Created with IntelliJ IDEA.
 */
class EventData(id: String, name: String, description: String)

object EventData {
  def newId = Random.alphanumeric.take(20).mkString

  def apply(name: String, description: String) = new EventData(newId, name, description)
}
