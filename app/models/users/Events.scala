package models.users

/**
 *
 * User: mateusz
 * Date: 06.03.2014
 * Time: 21:15
 * Created with IntelliJ IDEA.
 */
object Events {
  case class Register(login: String, userData: UserData, password: String)
  trait RegistrationResult
  case object RegistrationSuccessful extends RegistrationResult
  case object RegistrationFailed extends RegistrationResult

  case class Update(login: String, userData: UserData)
  case class PasswordChange(login: String, oldPassword: String, password: String)
  trait PasswordChangeResult
  case object SuccessfulPasswordChange extends PasswordChangeResult
  case object PasswordChangeFailed extends PasswordChangeResult

  case class Authenticate(login: String, password: String)
  trait AuthenticationResult
  case class UserAuthenticated(userData: UserData) extends AuthenticationResult
  case object MissingLogin extends AuthenticationResult
  case object WrongPassword extends AuthenticationResult

  case object RequestUsersList
  case class UsersList(users: Map[String, UserData])

  case class RequestUser(userId: String)
  trait UserRequestResult
  case class UserFound(user: UserData) extends UserRequestResult
  case object UserNotFound extends UserRequestResult
}
