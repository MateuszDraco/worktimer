package models.users

import com.mdraco.redis.Redis
import scala.pickling._
import json._
import models.users.Events._
import scala.pickling.json.JSONPickle
import scala.Some

/**
 *
 * User: mateusz
 * Date: 06.03.2014
 * Time: 20:39
 * Created with IntelliJ IDEA.
 */
case class UserData(name: String, email: String, skype: String, phone: String, info: Map[String, String], roles: List[UserRole]) {

  //val login = email

  lazy val isAdmin = roles.contains(SiteAdmin)

}

trait UserRole
case object SiteAdmin extends UserRole
case object Supervisor extends UserRole

case object UserData {

  def admin(name: String, email: String, skype: String, phone: String, info: Map[String, String]) =
    new UserData(name, email, skype, phone, info, List[UserRole](SiteAdmin))

  def passwordChange(login: String, oldPassword: String, password: String): Boolean = authenticate(login, oldPassword) match {
    case UserAuthenticated(data) =>
      persist(dataKey(login), UserDoc(data, Password.secure(password)))
      true
    case _ => false
  }

  private def users = "users"
  private def dataKey(login: String) = login

  private case class UserDoc(userData: UserData, passwordHash: String)

  def register(login:String, userData: UserData, password: String): Boolean = {
    val key = dataKey(login)
    val fromRedis: Option[String] = Redis.hashGet(users, key)
    fromRedis.map(x => false).getOrElse {
      val passwordHash = Password.secure(password)
      persist(key, UserDoc(userData, passwordHash))
      true
    }
  }

  private def persist(key: String, doc: UserDoc) = Redis.hashSet(users, key, doc.pickle.value)

  private def fetch(data: String): UserDoc = JSONPickle(data).unpickle[UserDoc]

  def get(login: String): Option[UserData] = Redis.hashGet(users, dataKey(login)) match {
    case Some(data) => Some(fetch(data).userData)
    case None => None
  }

  def authenticate(login: String, password: String): AuthenticationResult =
    Redis.hashGet(users, dataKey(login)) match {
      case Some(data) =>
        val doc = fetch(data)
        val passwordHash = Password.secure(password)
        if (doc.passwordHash == passwordHash)
          UserAuthenticated(doc.userData)
        else
          WrongPassword
      case None => MissingLogin
    }

  def set(login: String, userData: UserData): Boolean = Redis.hashGet(users, dataKey(login)) match {
    case None => false
    case Some(data) =>
      val oldDoc = fetch(data)
      val newDoc = UserDoc(userData, oldDoc.passwordHash)
      Redis.hashSet(users, dataKey(login), newDoc.pickle.value)
      true
  }

  def all(): Map[String, UserData] = //Redis.hashValues(users).map(data => fetch(data).userData)
    Redis.hashKeys(users).foldLeft(Map[String, UserData]()) { (map, login) =>
      get(login) match {
        case None => map
        case Some(data) => map + (login -> data)
      }
    }

  //implicit def userPickler(implicit pf: PickleFormat) =
  //  new UserPickler
}

/*
class UserPickler(implicit val format: PickleFormat) //, aTypeTag: FastTypeTag[Set[UserRole]], aPickler: SPickler[Set[UserRole]], aUnpickler: Unpickler[Set[UserRole]])
  extends SPickler[UserData] with Unpickler[UserData] {

  private val stringUnpickler = implicitly[Unpickler[String]]

  override def pickle(picklee: UserData, builder: PBuilder) = {
    builder.beginEntry(picklee)

    builder.putField("name",
      b => b.hintTag(FastTypeTag.ScalaString).beginEntry(picklee.name).endEntry()
    )
    builder.putField("email",
      b => b.hintTag(FastTypeTag.ScalaString).beginEntry(picklee.email).endEntry()
    )
    builder.putField("skype",
      b => b.hintTag(FastTypeTag.ScalaString).beginEntry(picklee.skype).endEntry()
    )

    builder.putField("roles",
      b => {
        b.hintTag(aTypeTag)
        aPickler.pickle(picklee.roles, b)
      }
    )

    builder.endEntry()
  }

  override def unpickle(tag: => FastTypeTag[_], reader: PReader): UserData = {
    reader.hintTag(FastTypeTag.ScalaString)
    val tag1 = reader.beginEntry()
    val name = stringUnpickler.unpickle(tag1, reader).asInstanceOf[String]
    reader.endEntry()

    reader.hintTag(FastTypeTag.ScalaString)
    val tag2 = reader.beginEntry()
    val email = stringUnpickler.unpickle(tag2, reader).asInstanceOf[String]
    reader.endEntry()

    reader.hintTag(FastTypeTag.ScalaString)
    val tag3 = reader.beginEntry()
    val skype = stringUnpickler.unpickle(tag3, reader).asInstanceOf[String]
    reader.endEntry()

    reader.hintTag(aTypeTag)
    val aTag = reader.beginEntry()
    val roles = aUnpickler.unpickle(aTag, reader).asInstanceOf[Set[UserRole]]
    reader.endEntry()

    UserData(name, email, skype, roles)
  }

}
*/
