package models.users

import akka.actor.Actor
import models.users.Events._
import models.users.Events.Register
import models.users.Events.Authenticate

/**
 *
 * User: mateusz
 * Date: 06.03.2014
 * Time: 22:02
 */
class UserActor extends Actor {

  override def receive: Actor.Receive = {

    case Register(login, userData, password) =>
      val data = if (UserData.register(login, userData, password)) {
        RegistrationSuccessful
      } else {
        RegistrationFailed
      }

      sender ! data

    case Authenticate(login, password) =>
      sender ! UserData.authenticate(login, password)

    case Update(login, userData) =>
      UserData.set(login, userData)

    case PasswordChange(login, oldPassword, password) =>
      val result: PasswordChangeResult = if (UserData.passwordChange(login, oldPassword, password)) SuccessfulPasswordChange else PasswordChangeFailed
      sender ! result

    case RequestUsersList =>
      sender ! UsersList(UserData.all)

    case RequestUser(userId) =>
      sender ! UserData.get(userId).map(u => UserFound(u)).getOrElse(UserNotFound)

  }
}
