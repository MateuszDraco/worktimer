package models.users

import javax.crypto.SecretKeyFactory
import javax.crypto.spec.PBEKeySpec
import java.math.BigInteger

/**
 *
 * User: mateusz
 * Date: 07.03.2014
 * Time: 07:24
 * Created with IntelliJ IDEA.
 */
object Password {
  val factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1")
  val salt = "~=~;+&34N&5)3G*28&$w".getBytes()
  val iterations = 1000

  def secure(password: String) : String = {
    val spec = new PBEKeySpec(password.toCharArray, salt, iterations, 64 * 8)
    val hash = factory.generateSecret(spec).getEncoded
    new BigInteger(1, hash).toString(16)
  }
}
