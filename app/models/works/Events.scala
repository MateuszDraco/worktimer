package models.works

import models.users.UserData

/**
 *
 * User: mateusz
 * Date: 08.03.2014
 * Time: 14:29
 * Created with IntelliJ IDEA.
 */
object Events {
  case class BeginWork(user: String)
  case class FinishWork(user: String)

  case class RequestWorkState(user: String)
  case class RequestedWorkState(state: WorkState)

  case class RequestWorkStates(users: List[String])
  case class RequestedWorkStates(states: Map[String, WorkState])
}
