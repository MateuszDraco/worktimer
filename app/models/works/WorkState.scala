package models.works

import models.users.UserData
import com.mdraco.redis.Redis
import java.util.Date
import scala.pickling._
import json._
import scala.util.Random

/**
 *
 * User: mateusz
 * Date: 10.03.2014
 * Time: 20:39
 * Created with IntelliJ IDEA.
 */
trait WorkState

case object UnknownWorkState extends WorkState
case class StartedWork(id: String, stamp: Date) extends WorkState
case class FinishedWork(id: String, stamp: Date) extends WorkState

object WorkState {

  private def newId = Random.alphanumeric.take(20).mkString

  private def workKey(userId: String) = s"user::works::${userId}"

  private def fetch(data: String): WorkState = JSONPickle(data).unpickle[WorkState]

  private def persist(state: WorkState): String = state.pickle.value

  private def markState(userId: String, state: WorkState) = Redis.listPush(workKey(userId), persist(state))

  def getState(user: String): WorkState = Redis.listRange(workKey(user), 0, 0) match {
    case List(data: String) => fetch(data)
    case _ => UnknownWorkState
  }

  //def setState(user: UserData, state: WorkState) = Redis.set(workKey(user), persist(state))
  def start(user: String) = getState(user) match {
    case StartedWork(id, stamp) =>
    case _ => markState(user, StartedWork(newId, new Date()))
  }

  def finish(user: String) = getState(user) match {
    case StartedWork(id, stamp) => markState(user, FinishedWork(newId, new Date()))
    case _ =>
  }
}
