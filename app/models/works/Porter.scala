package models.works

import com.mdraco.redis.Redis
import akka.actor.Actor
import models.works.Events._
import models.users.UserData
import models.works.Events.RequestWorkState
import models.works.Events.RequestedWorkState
import models.works.Events.BeginWork
import models.works.Events.FinishWork

/**
 *
 * User: mateusz
 * Date: 10.03.2014
 * Time: 20:42
 * Created with IntelliJ IDEA.
 */
class Porter extends Actor {


  override def receive: Actor.Receive = {
    case BeginWork(user) => WorkState.start(user)

    case FinishWork(user) => WorkState.finish(user)

    case RequestWorkState(user) => sender ! RequestedWorkState(WorkState.getState(user))

    case RequestWorkStates(users) =>
      sender ! RequestedWorkStates(users.foldRight(Map[String, WorkState]())((user, agg) => agg + (user -> WorkState.getState(user))))
  }
}
