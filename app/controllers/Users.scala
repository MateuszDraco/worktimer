package controllers

import akka.actor.ActorRef
import akka.util.Timeout
import models.works.UnknownWorkState
import scala.concurrent.{ExecutionContext, Future}
import ExecutionContext.Implicits.global
import akka.pattern.ask
import models.users.{SiteAdmin, UserRole, UserData}
import models.users.Events._
import play.api.data._
import play.api.data.Forms._
import models.users.Events.UsersList
import models.users.Events.UserFound
import scala.Some
import play.api.mvc.SimpleResult
import views.UserView

/**
 *
 * User: mateusz
 * Date: 16.03.2014
 * Time: 19:19
 * Created with IntelliJ IDEA.
 */
object Users extends SecuredController {
  override implicit val timeout: Timeout = Application.timeout
  override val users: ActorRef = Application.users
  override val works: ActorRef = Application.works

  case class UserFormData(name: String, email: String, skype: String, phone: String, isAdmin: Boolean, password: Option[String]) {
    def toData = new UserData(name, email, skype, phone, Map(), roles)

    private def roles: List[UserRole] = {
      if (isAdmin)
        List(SiteAdmin)
      else
        List()
    }
  }

  object UserFormData {
    def apply(u: UserData) = new UserFormData(
      u.name,
      u.email,
      u.skype,
      u.phone,
      u.isAdmin,
      None
    )
  }

  val userEdit = Form(mapping(
    "name" -> nonEmptyText,
    "email" -> email,
    "skype" -> text,
    "phone" -> text,
    "isAdmin" -> boolean,
    "password" -> tuple(
      "main" -> optional(text(minLength = 8)),
      "confirmation" -> optional(text)
    ).verifying(
      "hasła muszą się zgadzać", passwords => passwords._1 == passwords._2
    )
  ) {
    (name, email, skype, phone, admin, passwords) =>
      UserFormData(name, email, skype, phone, admin, passwords._1)
  } {
    user =>
      Some(user.name, user.email, user.skype, user.phone, user.isAdmin, (user.password, None))
  })

  def index = AdminAuthenticated.async { implicit request =>
    getLogged.flatMap { view =>
      (users ? models.users.Events.RequestUsersList).mapTo[UsersList].map { list =>
        Ok(views.html.admin.users.index(view, UnknownWorkState, list.users))
      }
    }
  }

  def edit(userId: String) = AdminAuthenticated.async { implicit request =>
    getLogged.flatMap { view =>
      (users ? models.users.Events.RequestUser(userId)).mapTo[UserRequestResult] map {
        case UserFound(userData) =>
          Ok(views.html.admin.users.edit(view, userId, userEdit.fill(UserFormData(userData))))
        case UserNotFound =>
          Redirect(routes.Users.index())
      }
    }
  }

  def update(userId: String) = AdminAuthenticated.async { implicit request =>
    getLogged.flatMap { view =>
      userEdit.bindFromRequest.fold(
        errors => {
          Future.successful(BadRequest(views.html.admin.users.edit(view, userId, errors)))
        },
        data => {
          users ! models.users.Events.Update(userId, data.toData)
          Future.successful(Redirect(routes.Users.index()))
        }
      )
    }
  }

  def create = AdminAuthenticated.async { implicit request =>
    getLogged.flatMap {
      view =>
        userEdit.bindFromRequest.fold(
          errors => {
            Future.successful(BadRequest(views.html.admin.users.edit(view, "", errors)))
          },
          data => {
                data.password match {
                  case None | Some("") =>
                    Future.successful(BadRequest(views.html.admin.users.edit(view, "", userEdit.fill(data).withError("password", "nie może być puste przy rejestracji"))))
                  case _ =>
                    createUser(view, data)
                }
          }
        )
    }
  }


  private def createUser(logged: UserView, data: Users.UserFormData): Future[SimpleResult] = {
    (users ? models.users.Events.Register(data.email, data.toData, data.password.get)).mapTo[RegistrationResult] map {
      case RegistrationSuccessful =>
        Redirect(routes.Users.index())
      case RegistrationFailed =>
        BadRequest(views.html.admin.users.edit(logged, "new", userEdit.fill(data).withGlobalError("Błędny login")))
    }
  }

  def newUser = AdminAuthenticated.async { implicit request =>
    getLogged.map { view =>
      Ok(views.html.admin.users.edit(view, "", userEdit))
    }
  }
}
