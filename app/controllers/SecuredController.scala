package controllers

import play.api.mvc._
import models.users.UserData
import akka.actor.ActorRef
import akka.util.Timeout

import akka.pattern.ask
import scala.concurrent.{ExecutionContext, Future}
import ExecutionContext.Implicits.global
import models.users.Events.{UserRequestResult, UserFound, RequestUser}
import models.works.Events.{RequestedWorkState, RequestWorkState}
import views.UserView


/**
 *
 * User: mateusz
 * Date: 12.03.2014
 * Time: 19:55
 * Created with IntelliJ IDEA.
 */
trait SecuredController extends Controller {

  val users: ActorRef
  val works: ActorRef

  implicit val timeout: Timeout

  class AuthenticatedRequest[A](val login: String, val user: UserData, request: Request[A]) extends WrappedRequest[A](request)

  private def onUnauthorized: SimpleResult = Redirect(routes.Application.logout())//] = Future.successful(Redirect(routes.Application.login()))

  trait Authenticated extends ActionBuilder[AuthenticatedRequest] {
    def invokeBlock[A](request: Request[A], block: (AuthenticatedRequest[A]) => Future[SimpleResult]) = {

      request.session.get("login").map {
        id =>
          (users ? RequestUser(id)).mapTo[UserRequestResult].flatMap {
            case UserFound(userData) => if (isValid(userData))
              block(new AuthenticatedRequest(id, userData, request))
              else Future.successful(onUnauthorized)
            case _ => Future.successful(onUnauthorized)
          }
      } getOrElse {
        Future.successful(onUnauthorized)
      }
    }

    def isValid(userData: UserData) = true
  }

  object Authenticated extends Authenticated

  object AdminAuthenticated extends Authenticated {

//    def isAdminRole(role: UserRole): Boolean = role match {
//      case SiteAdmin => true
//      case _ => false
//    }

    override def isValid(userData: UserData) = userData.isAdmin //foldLeft(false)((result, role) => result || isAdminRole(role))
  }

  def getLogged(implicit request: AuthenticatedRequest[AnyContent]) =
    (works ? RequestWorkState(request.login)).map {
      case RequestedWorkState(state) => UserView(request.login, request.user, state)
    }


}
