package controllers

import play.api.mvc._
import play.api.data._
import play.api.data.Forms._
import akka.actor.Props
import play.api.libs.concurrent.Akka
import akka.pattern.ask
import models.users.{UserData, UserActor}
import akka.util.Timeout
import scala.concurrent.duration._
import models.users.Events._
import play.api.Play.current
import scala.concurrent.{ExecutionContext, Future}
import ExecutionContext.Implicits.global
import models.works.{WorkState, UnknownWorkState, Porter}
import scala.language.postfixOps
import models.users.Events.UsersList
import models.works.Events.BeginWork
import models.works.Events.FinishWork
import models.users.Events.Register
import models.works.Events.RequestedWorkStates
import play.api.mvc.SimpleResult
import models.users.Events.UserAuthenticated
import models.works.Events.RequestWorkStates
import views.UserView

object Application extends SecuredController {

  implicit val timeout: Timeout = Timeout(5 seconds)

  val users = Akka.system.actorOf(Props[UserActor], name = "applicationUsers")
  val works = Akka.system.actorOf(Props[Porter], name = "usersWorks")

  private def getUsers = (users ? RequestUsersList).mapTo[UsersList]
  private def getWorkStates(userList: List[String]) = (works ? RequestWorkStates(userList)).mapTo[RequestedWorkStates]

  case class LoginData(login: String, password: String)

  val loginForm = Form(mapping(
    "login" -> nonEmptyText,
    "password" -> nonEmptyText
  )(LoginData.apply)(LoginData.unapply))

  def combine(users: Map[String, UserData], states: Map[String, WorkState]): List[UserView] =
    users.foldLeft(List[UserView]()) {
      case (list, (login, data)) => UserView(login, data, states(login)) :: list
    }.sortWith(_.profile.name < _.profile.name)


  def index = Authenticated.async { implicit request =>
    for {
      UsersList(userList) <- getUsers
      RequestedWorkStates(states) <- getWorkStates(userList.keys.toList)
    } yield Ok(views.html.index(UserView(request.login, request.user, UnknownWorkState), combine(userList, states)))
  }
//    getUsers.flatMap {
//      case UsersList(userList) => getWorkStates(userList.keys.toList)
//    } map {
//      case RequestedWorkStates(states) => {
//        val withoutMe = states//.filter((p) => p._1 != request.user)
//        states.find((pair) => pair._1 == request.user) match {
//          case Some((u, w)) =>
//            Ok(views.html.index(request.user, w, withoutMe))
//          case None =>
//            Ok(views.html.index(request.user, UnknownWorkState, withoutMe))
//        }
//      }
//    }
//  }

  def login = Action {
    Ok(views.html.login(loginForm))
  }

  def logout = Action {
    Redirect(routes.Application.login()).withNewSession
  }

  def authenticate = Action.async { implicit request =>
    (users ? models.users.Events.RequestUsersList).map {
      case UsersList(List()) =>
        println("registering first user")
        users ? Register("mateusz", UserData.admin("Mateusz Bednarski", "mateusz@dpksystem.pl", "mateuszdraco", "", Map()), "krowa")
      case _ =>
    }
    loginForm.bindFromRequest.fold(
      errors => {
        Future(BadRequest(views.html.login(errors)))
      },
      data => {
        doAuthenticate(data.login, data.password)
      }
    )
  }

  private def doAuthenticate(login: String, password: String)(implicit request: Request[AnyContent]): Future[SimpleResult] =
    (users ? models.users.Events.Authenticate(login, password)).mapTo[AuthenticationResult].map {
      case UserAuthenticated(userData) =>
        Redirect(routes.Application.index()).withSession(request.session + ("login" -> login))
      case _ => Redirect(routes.Application.login())
    }

  def startWork = Authenticated { implicit request =>
    works ! BeginWork(request.login)

    Redirect(routes.Application.index())
  }

  def endWork = Authenticated { implicit request =>
    works ! FinishWork(request.login)

    Redirect(routes.Application.index())
  }

  def admin = AdminAuthenticated.async { implicit request =>
    getLogged map {
      case view => Ok(views.html.admin.index(view))
    }
  }

}