name := "timer"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  //jdbc,
  //anorm,
  //cache,
  "com.mdraco" % "mdraco-redis-plugin_2.10" % "0.3.1",
  "org.scala-lang" %% "scala-pickling" % "0.8.0-SNAPSHOT"
)

resolvers += Resolver.sonatypeRepo("snapshots")

play.Project.playScalaSettings

scalacOptions += "-feature"

