#!/bin/bash

path='../../css/semantic'
su=packaged
sx=
#sx='.min'

cp -v $path/build/$su/css/semantic$sx.css public/stylesheets/
cp -v $path/build/$su/javascript/semantic$sx.js public/javascripts/
cp -v $path/build/$su/images/* public/images/
cp -v $path/build/$su/fonts/* public/fonts/


